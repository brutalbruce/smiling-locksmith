# Copyright 2013 Google Inc. All Rights Reserved.

"""Command to set properties."""

from googlecloudsdk.core import named_configs
from googlecloudsdk.core import properties

from googlecloudsdk.calliope import base
from googlecloudsdk.calliope import exceptions as c_exc
from googlecloudsdk.core import remote_completion


@c_exc.RaiseToolExceptionInsteadOf(properties.Error)
def RunSet(cmd, args, auto_upgrade):
  """Runs a config set command."""
  if auto_upgrade:
    named_configs.TryEnsureWriteableNamedConfig()

  prop = cmd.group.PropertyFromString(args.property)
  properties.PersistProperty(prop, args.value,
                             scope=properties.Scope.FromId(args.scope))


def Args(cmd_class, parser):
  """Adds args for this command."""
  cmd_class.group_class.SCOPE_FLAG.AddToParser(parser)
  property_arg = parser.add_argument(
      'property',
      metavar='SECTION/PROPERTY',
      help='The property to be set. Note that SECTION/ is optional while '
      'referring to properties in the core section.')
  property_arg.completer = cmd_class.group_class.PropertiesCompleter
  value_arg = parser.add_argument(
      'value',
      help='The value to be set.')
  cli = cmd_class.GetCLIGenerator()
  collection = 'cloudresourcemanager.projects'
  value_arg.completer = (remote_completion.RemoteCompletion.
                         GetCompleterForResource(collection, cli,
                                                 'alpha.projects'))

DETAILED_HELP = {
    'DESCRIPTION': '{description}',
    'EXAMPLES': """\
        To set the project property in the core section, run:

          $ {command} project myProject

        To set the zone property in the compute section, run:

          $ {command} compute/zone zone3
        """,
}


@base.ReleaseTracks(base.ReleaseTrack.GA)
class Set(base.Command):
  """Edit Google Cloud SDK properties.

  Set the value for an option, so that Cloud SDK tools can use them as
  configuration.
  """

  detailed_help = DETAILED_HELP

  @staticmethod
  def Args(parser):
    """Adds args for this command."""
    Args(Set, parser)

  def Run(self, args):
    RunSet(self, args, auto_upgrade=False)


@base.ReleaseTracks(base.ReleaseTrack.ALPHA, base.ReleaseTrack.BETA)
class SetWithUpgrade(base.Command):
  """Edit Google Cloud SDK properties.

  Set the value for an option, so that Cloud SDK tools can use them as
  configuration.
  """

  detailed_help = DETAILED_HELP

  @staticmethod
  def Args(parser):
    """Adds args for this command."""
    Args(SetWithUpgrade, parser)

  def Run(self, args):
    RunSet(self, args, auto_upgrade=True)
