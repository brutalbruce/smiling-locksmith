<?php

/* 
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

?>



<div id="exampleSection">
    <h1>
        Smiling Locksmith Security Solutions 
    </h1>  
    <div class="container">
    <p>
        Lost your keys to your car? 
    </p>
    <div id="lostCarKey" class="col-sm-offset-1 col-sm-4">     
        <div class="view view-tenth">        
            <img width="100%" height="auto" src="/views/img/car-key-replacement.jpg" >
                    <div class="mask">
                        <h2>Lost Car Keys</h2>
                        <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        <a href="#" class="info">Read More</a>
                    </div>
                </div>
    </div>
    <div id="lockedOut" class="col-sm-offset-1 col-sm-4">
         <div class="view view-tenth">        
             <img width="100%" height="auto" src="/views/img/house-key.jpg" >
                    <div class="mask">
                        <h2>Locked Out!?</h2>
                        <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        <a href="#" class="info">Read More</a>
                    </div>
         </div>
    </div>
    </div>
</div>