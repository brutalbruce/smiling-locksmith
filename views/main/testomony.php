<?php

/* 
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
?>

<div id="testimonialHeader">
    <h1> Customer Testimonies and Feedback </h1>
    
    
</div>

<div id="testimoniesCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#testimoniesCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#testimoniesCarousel" data-slide-to="1"></li>
    <li data-target="#testimoniesCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
        <img src="/views/img/icons/svgs/grin.svg" alt="...">
      <div class="carousel-caption">
      
          <p><?php echo file_get_contents("views/img/icons/svgs/quotes-left.svg") ?> This guys, great! I was so worried that it was going to
      be so expensive and long but he did it right away and for not nearly what I had feared! Thanks Guy!
        
          <sub> Rick James</sub>  </p>
          <?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
          <?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
          <?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
<?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
          <?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
      <?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
      </div>
    </div>
    <div class="item">
        <img height="100%" width="auto" src="/views/img/icons/svgs/bubbles3.svg" alt="...">
      <div class="carousel-caption">
        
          <p><?php echo file_get_contents("views/img/icons/svgs/quotes-left.svg") ?> I had a really complex problem and this dude 
              just did not give up on me, he found a fix that made everything mix just right. I would hire him again, for real! 
        
          <sub>- Dr. Dre</sub>  </p>
          <?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
          <?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
          <?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
<?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
          <?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
      <?php echo file_get_contents("views/img/icons/svgs/star-empty.svg") ?>
      </div>
    </div>
      <div class="item">
          <img height="100%" width="auto" src="/views/img/icons/svgs/skull.svg" alt="...">
      <div class="carousel-caption">
        
          <p><?php echo file_get_contents("views/img/icons/svgs/quotes-left.svg") ?> I am an evil master mind and I was locked out of my lab at a critical time.
              Good thing this guy was around I tell you, things would be very different if he haden't had opened my door in time.. and of course I have to give 
              a bad rating, I am evil. 
        
          <sub>- Dr. Nefarious</sub>  </p>
          <?php echo file_get_contents("views/img/icons/svgs/star-full.svg") ?>
          <?php echo file_get_contents("views/img/icons/svgs/star-empty.svg") ?>
          <?php echo file_get_contents("views/img/icons/svgs/star-empty.svg") ?>
<?php echo file_get_contents("views/img/icons/svgs/star-empty.svg") ?>
          <?php echo file_get_contents("views/img/icons/svgs/star-empty.svg") ?>
      <?php echo file_get_contents("views/img/icons/svgs/star-empty.svg") ?>
      </div>
    </div>
   
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#testimoniesCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#testimoniesCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
testimonies