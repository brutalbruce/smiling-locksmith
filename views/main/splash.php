<?php
/*
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
?>
<div class="jumbotron">

</div>
<div id="sLocksmithBannerContent">

        <h1 class="primary-font-color-light font-effect-3d noBreak">S m i l i n g &nbsp; &nbsp; L <img id="bannerLock" width="auto" height="54px" src="/views/img/icons/svgs/slFullLogoSVG.svg" alt="Smiling Locksmith icon"> c k s m i t h</h1>

    <div id="slBannerNameSubText">
        <strong class="noBreak">Your Local Lock and Key Solutions</strong>
    </div>
    <br>
    <br>
    <a href="tel:+19713001177" id="phoneNumberAnchor" class="noBreak"><span id="phoneIcon" ><?php echo file_get_contents("views/img/icons/svgs/phone.svg"); ?> </span> <h2 id="sLocksmithBannerPhoneNumber" class="text-center noBreak"> ( 9 7 1 ) 3 0 0 - 1 1 7 7 </h2></a>

    <div class="text-center font-white ">Proudly Serving : Tigard | Beaverton | Tulatain |Lake Oswego | and others just South of Portland </div>
    <br>
    <p>
        <button class="btn text-center primary-font-color-light " type="button"  data-toggle="modal" data-target="#sLocksmithServiceModal" role="button">
            P r e s s &nbsp;  f o r  &nbsp; I m m e d i a t e &nbsp;  S e r v i c e
        </button>>
    </p>


</div>