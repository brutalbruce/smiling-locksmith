# Copyright 2015 Google Inc. All Rights Reserved.
"""Code that's shared between multiple health-checks subcommands."""
from googlecloudsdk.calliope import arg_parsers
from googlecloudsdk.calliope import exceptions


THRESHOLD_UPPER_BOUND = 10
THRESHOLD_LOWER_BOUND = 1
TIMEOUT_UPPER_BOUND_SEC = 300
TIMEOUT_LOWER_BOUND_SEC = 1
CHECK_INTERVAL_UPPER_BOUND_SEC = 300
CHECK_INTERVAL_LOWER_BOUND_SEC = 1


def AddProtocolAgnosticCreationArgs(parser, protocol_string):
  """Adds parser arguments common to creation for all protocols."""

  check_interval_sec = parser.add_argument(
      '--check-interval',
      help='How often to run the check. Default is 5s.',
      type=arg_parsers.Duration(),
      default='5s')
  check_interval_sec.detailed_help = """\
      How often to perform a health check for an instance. For example,
      specifying ``10s'' will run the check every 10 seconds. Valid units
      for this flag are ``s'' for seconds and ``m'' for minutes.
      The default value is ``5s''.
       """

  timeout_sec = parser.add_argument(
      '--timeout',
      help='How long to wait until check is a failure. Default is 5s.',
      type=arg_parsers.Duration(),
      default='5s')
  timeout_sec.detailed_help = """\
      If Google Compute Engine doesn't receive a healthy response from the
      instance by the time specified by the value of this flag, the health
      check request is considered a failure. For example, specifying ``10s''
      will cause the check to wait for 10 seconds before considering the
      request a failure.  Valid units for this flag are ``s'' for seconds and
      ``m'' for minutes.  The default value is ``5s''.
      """

  unhealthy_threshold = parser.add_argument(
      '--unhealthy-threshold',
      help='Consecutive failures to mark instance unhealthy. Default is 2.',
      type=int,
      default=2)
  unhealthy_threshold.detailed_help = """\
      The number of consecutive health check failures before a healthy
      instance is marked as unhealthy. The default is 2.
      """

  healthy_threshold = parser.add_argument(
      '--healthy-threshold',
      help='Consecutive successes to mark instance healthy. Default is 2.',
      type=int,
      default=2)
  healthy_threshold.detailed_help = """\
      The number of consecutive successful health checks before an
      unhealthy instance is marked as healthy. The default is 2.
      """

  parser.add_argument(
      '--description',
      help=('An optional, textual description for the ' + protocol_string +
            ' health check.'))

  parser.add_argument(
      'name',
      help='The name of the ' + protocol_string + ' health check.')


def AddProtocolAgnosticUpdateArgs(parser, protocol_string):
  """Adds parser arguments common to update subcommand for all protocols."""

  check_interval_sec = parser.add_argument(
      '--check-interval',
      help='How often to run the check.',
      type=arg_parsers.Duration())
  check_interval_sec.detailed_help = """\
      How often to perform a health check for an instance. For example,
      specifying ``10s'' will run the check every 10 seconds. Valid units
      for this flag are ``s'' for seconds and ``m'' for minutes.
      """

  timeout_sec = parser.add_argument(
      '--timeout',
      help='How long to wait until check is a failure.',
      type=arg_parsers.Duration())
  timeout_sec.detailed_help = """\
      If Google Compute Engine doesn't receive a healthy response from the
      instance by the time specified by the value of this flag, the health
      check request is considered a failure. For example, specifying ``10s''
      will cause the check to wait for 10 seconds before considering the
      request a failure.  Valid units for this flag are ``s'' for seconds and
      ``m'' for minutes.
      """

  unhealthy_threshold = parser.add_argument(
      '--unhealthy-threshold',
      help='Consecutive failures to mark instance unhealthy.',
      type=int)
  unhealthy_threshold.detailed_help = """\
      The number of consecutive health check failures before a healthy
      instance is marked as unhealthy.
      """

  healthy_threshold = parser.add_argument(
      '--healthy-threshold',
      help='Consecutive successes to mark instance healthy.',
      type=int)
  healthy_threshold.detailed_help = """\
      The number of consecutive successful health checks before an
      unhealthy instance is marked as healthy.
      """

  parser.add_argument(
      '--description',
      help=('A textual description for the ' + protocol_string +
            ' health check. Pass in an empty string to unset.'))

  parser.add_argument(
      'name',
      help='The name of the ' + protocol_string + ' health check.')


def AddHttpRelatedCreationArgs(parser):
  """Adds parser arguments for creation related to HTTP."""

  host = parser.add_argument(
      '--host',
      help='The value of the host header used by the HTTP health check.')
  host.detailed_help = """\
      The value of the host header used in this HTTP health check request.
      By default, this is empty and Google Compute Engine automatically sets
      the host header in health requests to the same external IP address as
      the forwarding rule associated with the target pool.
      """

  port = parser.add_argument(
      '--port',
      help='The TCP port number for the health request. Default is 80.',
      type=int,
      default=80)
  port.detailed_help = """\
      The TCP port number that this health check monitors. The default value
      is 80.
      """

  port_name = parser.add_argument(
      '--port-name',
      help='The port name for the health request. Default is empty.')
  port_name.detailed_help = """\
      The port name that this health check monitors. By default, this is
      empty.
      """

  request_path = parser.add_argument(
      '--request-path',
      help="The request path for the health check. Default is ``/''.",
      default='/')
  request_path.detailed_help = """\
      The request path that this health check monitors. For example,
      ``/healthcheck''. The default value is ``/''.
      """


def AddHttpRelatedUpdateArgs(parser):
  """Adds parser arguments for update subcommand related to HTTP."""

  host = parser.add_argument(
      '--host',
      help='The value of the host header used by the HTTP health check.')
  host.detailed_help = """\
      The value of the host header used in this HTTP health check request.
      By default, this is empty and Google Compute Engine automatically sets
      the host header in health requests to the same external IP address as
      the forwarding rule associated with the target pool. Setting this to
      an empty string will clear any existing host value.
      """

  port = parser.add_argument(
      '--port',
      help='The TCP port number for the health request.',
      type=int)
  port.detailed_help = """\
      The TCP port number that this health check monitors.
      """

  port_name = parser.add_argument(
      '--port-name',
      help='The port name for the health request. Default is empty.')
  port_name.detailed_help = """\
      The port name that this health check monitors. By default, this is
      empty.
      """

  request_path = parser.add_argument(
      '--request-path',
      help='The request path for the health check.')
  request_path.detailed_help = """\
      The request path that this health check monitors. For example,
      ``/healthcheck''.
      """


def CheckProtocolAgnosticArgs(args):
  """Raises exception if any protocol-agnostic args are invalid."""

  if (args.check_interval is not None
      and (args.check_interval < CHECK_INTERVAL_LOWER_BOUND_SEC
           or args.check_interval > CHECK_INTERVAL_UPPER_BOUND_SEC)):
    raise exceptions.ToolException(
        '[--check-interval] must not be less than {0} second or greater '
        'than {1} seconds; received [{2}] seconds.'.format(
            CHECK_INTERVAL_LOWER_BOUND_SEC, CHECK_INTERVAL_UPPER_BOUND_SEC,
            args.check_interval))

  if (args.timeout is not None
      and (args.timeout < TIMEOUT_LOWER_BOUND_SEC
           or args.timeout > TIMEOUT_UPPER_BOUND_SEC)):
    raise exceptions.ToolException(
        '[--timeout] must not be less than {0} second or greater than {1} '
        'seconds; received: [{2}] seconds.'.format(
            TIMEOUT_LOWER_BOUND_SEC, TIMEOUT_UPPER_BOUND_SEC, args.timeout))

  if (args.healthy_threshold is not None
      and (args.healthy_threshold < THRESHOLD_LOWER_BOUND
           or args.healthy_threshold > THRESHOLD_UPPER_BOUND)):
    raise exceptions.ToolException(
        '[--healthy-threshold] must be an integer between {0} and {1}, '
        'inclusive; received: [{2}].'.format(THRESHOLD_LOWER_BOUND,
                                             THRESHOLD_UPPER_BOUND,
                                             args.healthy_threshold))

  if (args.unhealthy_threshold is not None
      and (args.unhealthy_threshold < THRESHOLD_LOWER_BOUND
           or args.unhealthy_threshold > THRESHOLD_UPPER_BOUND)):
    raise exceptions.ToolException(
        '[--unhealthy-threshold] must be an integer between {0} and {1}, '
        'inclusive; received [{2}].'.format(THRESHOLD_LOWER_BOUND,
                                            THRESHOLD_UPPER_BOUND,
                                            args.unhealthy_threshold))
