<?php
/*
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is ic language governing permissions and
 * limitations under the License.
 */
?>
<nav class="nav navbar-inverse" id="sLNavList">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container-fluid">
        <div class="navbar-header">
            <button id="menuToggle" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNavDiv" aria-expanded="false">
                <img width="45px" src="/views/img/menuIcon-transparent.png" alt="nav toggle">
            </button>
                <a class="navbar-brand"  href="#sLocksmithSplash">
                    <img class="img-circle" id="sLocksmithNavLogo" width="45px" height="auto" src="/views/img/lockSVG-transparrent.png" alt="Smilling Locksmith Logo">
                </a>
    
            <div class="collapse navbar-collapse" >
                <ul class="navbar nav-pills nav-justified " >
                    <li><a href="#sLocksmithServices">Services <?php echo file_get_contents("views/img/icons/svgs/tools.svg") ?></a></li>
                    <li><a href="#sLocksmithExamples">Specializations <?php echo file_get_contents("views/img/icons/svgs/target.svg") ?></a></li>
                    <li><a href="#sLocksmithTestomnies">Smiling Customers<?php echo file_get_contents("views/img/icons/svgs/bubbles4.svg") ?> </a></li>
                    <li><a href="#sLocksmithCoverageArea">Service Area <?php echo file_get_contents("views/img/icons/svgs/location.svg") ?></a></li>
                
                    <li> <button data-toggle="modal" data-target="#sLocksmithServiceModal" >
                    Request Service
                </button>
                    </li>
                
                
                </ul>
               
            </div><!-- /.navbar-collapse -->
        </div>
    </div>
    <!-- /.container-fluid -->
</nav>
