<!doctype html>
<html class="no-js"  lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Smiling Locksmith</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php include_once 'iconLinks.php'; ?>

        <script type="text/javascript" src="js/vendor/modernizr.custom.17825.js"></script>
        <link  type="text/css" rel="stylesheet" href="css/normalize.css">

        <!-- CDNs todo replace with downlaoded versions on live -->
        <script src="js/vendor/jquery-2.1.4.min.js" type="text/javascript"></script>
      <!-- <script src="//code.jquery.com/jquery-1.11.3.min.js"></script> -->
        <link  type="text/css" rel="stylesheet" href="/vendor/twbs/bootstrap/dist/css/bootstrap-theme.min.css">
        <link type="text/css" rel="stylesheet" href="/vendor/twbs/bootstrap/dist/css/bootstrap.min.css">


        <script src="js/vendor/jquery.sticky.js"></script>
        <script src="/views/styles/js/collapsable.js" ></script>
        <script>
            $(window).load(function () {
                $("#sLNavList").sticky({topSpacing: 0});
            });
                </script>

        <script type="text/javascript" src="/js/vendor/angular.js"></script>
        <script type="text/javascript" src="/controllers/slApp.js"></script>

        <link type="text/css" rel="stylesheet" href="views/styles/stylesheets/main.css">


    </head>




    <body  ng-app="slApp" data-spy="scroll" data-target="#sLNavList">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="sLocksmithSection" id="sLocksmithSplash">
            <?php include_once 'views/main/splash.php'; ?>

        </div>

        <div class="sLocksmithSection" id="sLocksmithLayout">
            <?php include_once 'views/layout/navbar.php'; ?>
        </div>

        <div class="sLocksmithSection" id="sLocksmithServices">
            <?php include_once 'views/main/services.php'; ?>
        </div>

        <img id="serviceExampleBreakImg" src="/views/img/dial-lock.png" alt="a picture of a lock seperating the services and example section.">

        <div class="sLocksmithSection" id="sLocksmithExamples">
            <?php include_once 'views/main/examples.php'; ?>

        </div>

        <div  class="sLocksmithSection" id="sLocksmithTestomnies">
            <?php include_once 'views/main/testomony.php'; ?>

        </div>
        <div class="sLocksmithSection" id="sLocksmithCoverageArea">
            <?php // include_once 'views/main/area.php'; ?>

            <img src="/views/img/tigard.gif" alt="map of tigard">
        </div>
        <footer id="sLocksmithFooter">
            <?php include_once 'views/layout/footer.php'; ?>
        </footer>

        <!-- Modal -->
        <div class="modal fade" id="sLocksmithServiceModal" tabindex="-1" role="dialog" aria-labelledby="serviceModal">

            <h1> Modal </h1>
            <div class="modal-dialog" role="document">
                <div class="modal-content" ng-controller="ARMController">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="serviceModal">Immediate Service Instant Messenger</h4>
                    </div>
                    <div class="modal-body">


                        <arm></arm>


                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Get Service</button>
                    </div>
                </div>
            </div>
        </div>       

    </div>







      <!--  <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = 'https://www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-57705753-1', 'auto');
            ga('send', 'pageview');
        </script>

    -->



    <script>
        $(function () {
            $('a[href*=#]:not([href=#])').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }

            });
        });</script>

    <script src="views/styles/js/smoothScroll.js" type="text/javascript"></script>


    <script>
        $('#sLocksmithServiceModal').on('shown.bs.modal', function () {
            $('#serviceNowBtn').focus();
        });

    </script>


    <script src="js/plugins.js"></script>
    <script src="/vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="views/styles/js/scrollSpy.js" type="text/javascript"></script>

</body>
</html>
