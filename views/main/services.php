<?php
/*
 * Copyright 2015 Simple Design <Rob.Xcog at xcogstudios@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
?>
<div id="servicesHeader">
</div>


<div id="servicesHeaderContents">
<h1 class="noBreak text-center">Smiling Locksmith Services </h1>
<p>
    Description of Services TODO...
</p>
</div>
</div>
<div id="serviceWrapper">
<div id="slServiceSection00">
    

<button  class="a-btn" type="button" data-toggle="collapse" data-target="#commercialAndResidential" aria-expanded="false" aria-controls="collapseExample">
	<span class="a-btn-text">Commercial and Residential</span> 
	<span class="a-btn-slide-text">We provide solutions for home and business</span>
        <span class="a-btn-icon-right"><span id="cRIconSpan"><?php echo file_get_contents("views/img/icons/svgs/office.svg"); ?></span></span>
</button>



    <div class="collapse" id="commercialAndResidential">
        <div class="well ">
            
            <div class="container">

                <div class="row">
                     <div class="panel panel-success col-sm-3 ">                
                         <div class="panel-heading">                    
                             <h3 class="panel-title">Lockouts<?php echo file_get_contents("views/img/icons/svgs/key.svg"); ?></h3>
                        
               
                         </div>                
                         <div class="panel-body">                    
                             Panel content                
                         </div>
            
                     </div>
          
                <div class="panel panel-primary col-sm-offset-1 col-sm-2 ">                
                    <div class="panel-heading">
                        <h3 class="panel-title">Re-Key</h3>
                    </div>
                    <div class="panel-body">
                        Panel content
                    </div>
                </div> 
                <div class="panel panel-warning col-sm-offset-1 col-sm-3 ">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            New-Lock Installations
                        </h3>
                    </div>
                    <div class="panel-body">
                        Panel content
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="panel panel-info col-sm-offset-1 col-sm-3">
                <div class="panel-heading">
                    <h3 class="panel-title">Diagnostic & Repair</h3>
                </div>
                <div class="panel-body">
                    Panel content
                </div>
            
                    </div>
                        
            <div class="panel panel-default col-sm-offset-1 col-sm-3 " >
                <div class="panel-heading">
                    <h3 class="panel-title">File Cabinet & Mailbox</h3>
                </div>
                <div class="panel-body">
                    Panel content
                </div>
            </div>  
                    
                </div>
           
        
      

            </div>
        </div>
    </div>

</div>


<div id="slServiceSection01">


<button  class="a-btn" id="servicesAutoBtn" type="button" data-toggle="collapse" data-target="#automotive" aria-expanded="false" aria-controls="collapseExample">
	<span class="a-btn-text">Automotive </span> 
	<span class="a-btn-slide-text">Automobile security is our specialty, no key, no problem.</span>
	<span class="a-btn-icon-right"><span><?php echo file_get_contents("views/img/icons/svgs/car.svg"); ?></span></span>
</button>

    <div class="collapse" id="automotive">
        <div class="well">
            <div class="container">
            <div class="panel panel-primary col-sm-offset-1 col-sm-3">
                <div class="panel-heading">
                    <h3 class="panel-title">Lockouts</h3>
                </div>
                <div class="panel-body">
                    Panel content
                </div>
            </div>
            <div class="panel panel-success col-sm-offset-1 col-sm-3">
                <div class="panel-heading">
                    <h3 class="panel-title">Re-Key</h3>
                </div>
                <div class="panel-body">
                    Panel content
                </div>
            </div>
            <div class="panel panel-info col-sm-offset-1 col-sm-3">
                <div class="panel-heading">
                    <h3 class="panel-title">High Security Keys</h3>
                </div>
                <div class="panel-body">
                    Panel content
                </div>
            </div>
            <div class="panel panel-warning col-sm-offset-1 col-sm-3">
                <div class="panel-heading">
                    <h3 class="panel-title">Transponder Keys</h3>
                </div>
                <div class="panel-body">
                    Panel content
                </div>
            </div>
            <div class="panel panel-success col-sm-offset-1 col-sm-3">
                <div class="panel-heading">
                    <h3 class="panel-title">Valet Keys</h3>
                </div>
                <div class="panel-body">
                    Panel content
                </div>
            </div>
            <div class="panel panel-default col-sm-offset-1 col-sm-3">
                <div class="panel-heading">
                    <h3 class="panel-title">Remotes</h3>
                </div>
                <div class="panel-body">
                    Panel content
                </div>
            </div>
            <div class="panel panel-warning col-sm-offset-1 col-sm-3">
                <div class="panel-heading">
                    <h3 class="panel-title">Ignition Replacement/Repair</h3>
                </div>
                <div class="panel-body">
                    Panel content
                </div>
            </div>

</div>

        </div>
    </div>
</div>


</div>